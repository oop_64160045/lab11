package com.chanatda.week11;

public class Snake extends Animal implements Crawlable {
    public Snake(String name) {
        super(name,0);
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + "eat.");
    }

    @Override
    public String toString() {
        return "Snake (" + this.getName() + ") ";
    }

    @Override
    public void crawlable() {
        System.out.println(this + "crawlable.");
        
    }
}
