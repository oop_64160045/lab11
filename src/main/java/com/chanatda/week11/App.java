package com.chanatda.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");

        Plane boeing = new Plane("Boeing", "Rosaroi");

        Superman Clark = new Superman("Clark");

        Human man1 = new Human("Man");

        Bat bat = new Bat("Khangkhao");

        Cat cat = new Cat("Maeo");

        Dog dog = new Dog("Ma");

        Fish fish = new Fish("Pla");

        Rat rat = new Rat("Nu");

        Snake snake = new Snake("Ngu");

        Crocodile crocodile = new Crocodile("Chorakhe");

        Flyable[] flyables = { bird1, boeing, Clark, bat };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].fly();
            flyables[i].takeoff();
            flyables[i].landing();

        }
        Walkable[] walkables = { bird1, Clark, man1, cat, dog, rat, crocodile };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();

        }
        Animal[] animals = { bird1, Clark, man1, cat, bat, dog, fish, rat, snake, crocodile };
        for (int i = 0; i < animals.length; i++) {
            animals[i].eat();
            animals[i].sleep();
        }

        Swimable[] swimables = { Clark, man1, dog, fish, rat, crocodile };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        Crawlable[] crawlable = {snake, crocodile};
        for (int i = 0; i < crawlable.length; i++) {
            crawlable[i].crawlable();
        }
    }

}
