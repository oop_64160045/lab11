package com.chanatda.week11;

public class Crocodile extends Animal implements Swimable,Walkable,Crawlable {
    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this + "eat.");
    }

    @Override
    public String toString() {
        return "Crocodile (" + this.getName() + ") ";
    }

    @Override
    public void walk() {
        System.out.println(this + "walk.");
    }

    @Override
    public void run() {
        System.out.println(this + "run.");
    }

    @Override
    public void swim() {
        System.out.println(this + "swim.");
    }

    @Override
    public void crawlable() {
        System.out.println(this + "crawlable.");
        
    }
}
